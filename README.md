# mstakx task
..............
Technologies Used:
...................
Python, django, djangorestframework, postgressql


Steps to run the project (worked in linux platform ex:ubuntu OS)
.................................................................
1)Move to specific directroy ex: cd directory
2)issue the command "virtualenv -p python3 env"  you will see dirctory with name "env" after successfull execution
3)issue the command "source env/bin/activate" your environment will be activated
4)to clone the project issue the command "git clone https://nanjegowdabk998@bitbucket.org/nanjegowdabk998/mstakx.git" you will see dirctory called "mstakx" after finsihing cloning
5)Move to mstakx directory ex: cd "mstakx" you will see manage.py file and one more mstakx directory move to that directory using cd mstakx again
6)you will see settings.py file you can make neccessary database settings as per availability(should use postgres as database)
ex:    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mstakx',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }

7)move back one step where you will see mange.py file and requirements.txt file
8)to install all dependency issue the command "pip install -r requirements.txt"
9)once all dependencies installed issue the command "python manage.py makemigrations app1" to create migrate files for db
10)issue the command "python manage.py migrate" to make migrate files affect db
9)once all above steps completed issue the command "python manage.py runserver" by default it will run in 8000 port.
if you want to run in 8080 use command "python manage.py runserver 8080"
10)thats it you can access the api's as per the requirements given in document

sample url links for better accessing
.....................................

req1)http://localhost:8000/api/external-books?name=A Game of Thrones
req1)http://localhost:8000/api/external-books?name=A Clash of Kings


req2)list: http://localhost:8000/api/v1/books/  ("get" method to list all available records and "post" to post data to db from this url)
req2)list: http://localhost:8000/api/v1/books/1 ("get" method to get record with id=1 and "delete" method to delete record with id=1 
and "put/patch" method to update record for id=1)

Note: * PUT/PATCH methods you can see in the page-end
*Use Raw data tab to post/patch/put in the browser
*Postgres DB is used because arrayfield/listfield datatype available in it
