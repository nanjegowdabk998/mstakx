from django.db import models
from django.contrib.postgres.fields import ArrayField
from rest_framework import serializers


class Books(models.Model):
    """This models defines table books in postgres db"""
    name = models.CharField(max_length=100)
    isbn = models.CharField(max_length=80)
    authors = ArrayField(models.CharField(max_length=200))
    number_of_pages = models.IntegerField()
    publisher = models.CharField(max_length=150)
    country = models.CharField(max_length=100)
    release_date = models.DateField()


class BooksSerial(serializers.ModelSerializer):
    """This class defines serializer  to automate the CRUD process"""

    name = serializers.CharField(max_length=100)
    isbn = serializers.CharField(max_length=80)
    authors = serializers.ListField()
    number_of_pages = serializers.IntegerField()
    publisher = serializers.CharField(max_length=150)
    country = serializers.CharField(max_length=100)
    release_date = serializers.DateField()

    class Meta:
        """Used to set/get inner attributes of models class"""
        model = Books
        fields = ['id', 'name', 'isbn', 'authors', 'number_of_pages',
                  'publisher', 'country', 'release_date']
