"""Using the standard APITestCase to test end point"""
from rest_framework.test import APITestCase
IP_PROTOCOL = 'http://127.0.0.1:8000/api/'


class V1BooksTest(APITestCase):

    def test_method(self):
        url = IP_PROTOCOL+"v1/books/"
        data = {"name": "book1", "isbn": "2345234dwe",
                "authors": ["auth1", "auth2"], "number_of_pages": 100,
                "publisher": "sun micro", "country": "india",
                "release_date": "1990-10-02"}
        response = self.client.post(url, data, 'json')
        self.assertEquals(response.status_code, 201)
