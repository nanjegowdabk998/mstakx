"""This views will defined for serving client needs as per the requirements"""

import requests as req
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from .models import Books, BooksSerial
ICE_FIRE_RESULT = []


def get_details(arg):
    """
    This function is to get contents of Ice And Fire API
    results: variable to  store the extracted data
    """
    data = req.get(url='https://www.anapioficeandfire.com/api/books')
    arg += data.json()


get_details(ICE_FIRE_RESULT)


class BooksView(APIView):
    """This class will handle get request for different book names"""

    def get(self, request, name):
        """
        This method is to get details of specific book given as input if exists in Ice And Fire API
        request: client request object
        name: input name to find in global ICE_FIRE_RESULT  variable
        """
        try:
            name = name or request.GET['name'] or self.request.GET['name']
            available_names = {obj['name']: obj['url']
                               for obj in ICE_FIRE_RESULT}
            if name in available_names:
                not_req = ['povCharacters', 'characters', 'mediaType', 'url']
                obj_dic = req.get(url=available_names[name]).json()
                data = {key: obj_dic[key]
                        for key in obj_dic if key not in not_req}
            else:
                data = []
            ref = "success"
        except (KeyError, AttributeError):
            data = []
            ref = "failed"
        return Response({"status_code": status.HTTP_200_OK,
                         "status": ref, "data": data})


class V1Books(ModelViewSet):
    """
    This class for performing CRUD Operation as per the requirements
    """
    serializer_class = BooksSerial
    queryset = Books.objects.all()
