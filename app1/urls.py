from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import BooksView, V1Books

router = DefaultRouter()
router.register(r'v1/books', V1Books)

urlpatterns = [
    url('external-books(?P<name>)/$', BooksView.as_view()),
    path("", include(router.urls)),
]
